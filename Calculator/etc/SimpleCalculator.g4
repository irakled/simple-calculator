grammar SimpleCalculator;

@header {
package com.org.ir.calculator.antlr;

import java.util.HashMap;
}

@members {
HashMap map = new HashMap();
}

arithmeticExpression returns [int value]: 
	( 'let' LPAR s = STRINGCHAR ',' 
		n1 = numeric[$n1.text] ',' 
		{
			map.put($s.text, new Integer($n1.value));
		}
		n2 = numeric[$n2.text] RPAR 
		{
			$value = $n2.value;
		}
	| 'add' LPAR n1 = numeric[$n1.text] ',' n2 = numeric[$n2.text] RPAR {$value =  $n1.value + $n2.value;} 
	| 'sub' LPAR n1 = numeric[$n1.text] ',' n2 = numeric[$n2.text] RPAR {$value =  $n1.value - $n2.value;} 
	| 'div' LPAR n1 = numeric[$n1.text] ',' n2 = numeric[$n2.text] RPAR {$value =  $n1.value / $n2.value;} 
	| 'mult' LPAR n1 = numeric[$n1.text] ',' n2 = numeric[$n2.text] RPAR {$value =  $n1.value * $n2.value;} 
    )*
;

assignmentExpression returns [int value]: 
	'let' LPAR s = STRINGCHAR ',' 
	n1 = numeric[$n1.text] ',' 
	{
		map.put($s.text, new Integer($n1.value));
	}
	n2 = numeric[$n2.text] RPAR 
	{		
		$value = $n2.value;
	}
; 

numeric [String expr] returns [int value]: 
	e = assignmentExpression {$value = $e.value;}
	| n = NUMBER {$value = Integer.parseInt($n.text);} 
	| p = arithmeticExpression {$value = $p.value;} 
	| s = STRINGCHAR {
		Integer v = (Integer)map.get($s.text);
        if ( v!=null ) $value = v.intValue();       
    }
;

LPAR : '(' ;

RPAR : ')' ;

STRINGCHAR : ('a' .. 'z') | ('A' .. 'Z') ;

NUMBER :   '-'? INT ;

INT : '0' | [1-9] [0-9]*;

STRING : '"' (' '..'~')* '"';

WS : [ \t\r\n\u000C]+ -> skip ;