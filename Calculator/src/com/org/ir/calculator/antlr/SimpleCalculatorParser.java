// Generated from etc\SimpleCalculator.g4 by ANTLR 4.5.3

package com.org.ir.calculator.antlr;

import java.util.HashMap;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SimpleCalculatorParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, LPAR=7, RPAR=8, STRINGCHAR=9, 
		NUMBER=10, INT=11, STRING=12, WS=13;
	public static final int
		RULE_arithmeticExpression = 0, RULE_assignmentExpression = 1, RULE_numeric = 2;
	public static final String[] ruleNames = {
		"arithmeticExpression", "assignmentExpression", "numeric"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'let'", "','", "'add'", "'sub'", "'div'", "'mult'", "'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, "LPAR", "RPAR", "STRINGCHAR", 
		"NUMBER", "INT", "STRING", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SimpleCalculator.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


	HashMap map = new HashMap();

	public SimpleCalculatorParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ArithmeticExpressionContext extends ParserRuleContext {
		public int value;
		public Token s;
		public NumericContext n1;
		public NumericContext n2;
		public List<TerminalNode> LPAR() { return getTokens(SimpleCalculatorParser.LPAR); }
		public TerminalNode LPAR(int i) {
			return getToken(SimpleCalculatorParser.LPAR, i);
		}
		public List<TerminalNode> RPAR() { return getTokens(SimpleCalculatorParser.RPAR); }
		public TerminalNode RPAR(int i) {
			return getToken(SimpleCalculatorParser.RPAR, i);
		}
		public List<TerminalNode> STRINGCHAR() { return getTokens(SimpleCalculatorParser.STRINGCHAR); }
		public TerminalNode STRINGCHAR(int i) {
			return getToken(SimpleCalculatorParser.STRINGCHAR, i);
		}
		public List<NumericContext> numeric() {
			return getRuleContexts(NumericContext.class);
		}
		public NumericContext numeric(int i) {
			return getRuleContext(NumericContext.class,i);
		}
		public ArithmeticExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmeticExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).enterArithmeticExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).exitArithmeticExpression(this);
		}
	}

	public final ArithmeticExpressionContext arithmeticExpression() throws RecognitionException {
		ArithmeticExpressionContext _localctx = new ArithmeticExpressionContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_arithmeticExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(51);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5))) != 0)) {
				{
				setState(49);
				switch (_input.LA(1)) {
				case T__0:
					{
					setState(6);
					match(T__0);
					setState(7);
					match(LPAR);
					setState(8);
					((ArithmeticExpressionContext)_localctx).s = match(STRINGCHAR);
					setState(9);
					match(T__1);
					setState(10);
					((ArithmeticExpressionContext)_localctx).n1 = numeric((((ArithmeticExpressionContext)_localctx).n1!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n1.start,((ArithmeticExpressionContext)_localctx).n1.stop):null));
					setState(11);
					match(T__1);

								map.put((((ArithmeticExpressionContext)_localctx).s!=null?((ArithmeticExpressionContext)_localctx).s.getText():null), new Integer(((ArithmeticExpressionContext)_localctx).n1.value));
							
					setState(13);
					((ArithmeticExpressionContext)_localctx).n2 = numeric((((ArithmeticExpressionContext)_localctx).n2!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n2.start,((ArithmeticExpressionContext)_localctx).n2.stop):null));
					setState(14);
					match(RPAR);

								((ArithmeticExpressionContext)_localctx).value =  ((ArithmeticExpressionContext)_localctx).n2.value;
							
					}
					break;
				case T__2:
					{
					setState(17);
					match(T__2);
					setState(18);
					match(LPAR);
					setState(19);
					((ArithmeticExpressionContext)_localctx).n1 = numeric((((ArithmeticExpressionContext)_localctx).n1!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n1.start,((ArithmeticExpressionContext)_localctx).n1.stop):null));
					setState(20);
					match(T__1);
					setState(21);
					((ArithmeticExpressionContext)_localctx).n2 = numeric((((ArithmeticExpressionContext)_localctx).n2!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n2.start,((ArithmeticExpressionContext)_localctx).n2.stop):null));
					setState(22);
					match(RPAR);
					((ArithmeticExpressionContext)_localctx).value =   ((ArithmeticExpressionContext)_localctx).n1.value + ((ArithmeticExpressionContext)_localctx).n2.value;
					}
					break;
				case T__3:
					{
					setState(25);
					match(T__3);
					setState(26);
					match(LPAR);
					setState(27);
					((ArithmeticExpressionContext)_localctx).n1 = numeric((((ArithmeticExpressionContext)_localctx).n1!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n1.start,((ArithmeticExpressionContext)_localctx).n1.stop):null));
					setState(28);
					match(T__1);
					setState(29);
					((ArithmeticExpressionContext)_localctx).n2 = numeric((((ArithmeticExpressionContext)_localctx).n2!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n2.start,((ArithmeticExpressionContext)_localctx).n2.stop):null));
					setState(30);
					match(RPAR);
					((ArithmeticExpressionContext)_localctx).value =   ((ArithmeticExpressionContext)_localctx).n1.value - ((ArithmeticExpressionContext)_localctx).n2.value;
					}
					break;
				case T__4:
					{
					setState(33);
					match(T__4);
					setState(34);
					match(LPAR);
					setState(35);
					((ArithmeticExpressionContext)_localctx).n1 = numeric((((ArithmeticExpressionContext)_localctx).n1!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n1.start,((ArithmeticExpressionContext)_localctx).n1.stop):null));
					setState(36);
					match(T__1);
					setState(37);
					((ArithmeticExpressionContext)_localctx).n2 = numeric((((ArithmeticExpressionContext)_localctx).n2!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n2.start,((ArithmeticExpressionContext)_localctx).n2.stop):null));
					setState(38);
					match(RPAR);
					((ArithmeticExpressionContext)_localctx).value =   ((ArithmeticExpressionContext)_localctx).n1.value / ((ArithmeticExpressionContext)_localctx).n2.value;
					}
					break;
				case T__5:
					{
					setState(41);
					match(T__5);
					setState(42);
					match(LPAR);
					setState(43);
					((ArithmeticExpressionContext)_localctx).n1 = numeric((((ArithmeticExpressionContext)_localctx).n1!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n1.start,((ArithmeticExpressionContext)_localctx).n1.stop):null));
					setState(44);
					match(T__1);
					setState(45);
					((ArithmeticExpressionContext)_localctx).n2 = numeric((((ArithmeticExpressionContext)_localctx).n2!=null?_input.getText(((ArithmeticExpressionContext)_localctx).n2.start,((ArithmeticExpressionContext)_localctx).n2.stop):null));
					setState(46);
					match(RPAR);
					((ArithmeticExpressionContext)_localctx).value =   ((ArithmeticExpressionContext)_localctx).n1.value * ((ArithmeticExpressionContext)_localctx).n2.value;
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(53);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
			throw re;
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentExpressionContext extends ParserRuleContext {
		public int value;
		public Token s;
		public NumericContext n1;
		public NumericContext n2;
		public TerminalNode LPAR() { return getToken(SimpleCalculatorParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(SimpleCalculatorParser.RPAR, 0); }
		public TerminalNode STRINGCHAR() { return getToken(SimpleCalculatorParser.STRINGCHAR, 0); }
		public List<NumericContext> numeric() {
			return getRuleContexts(NumericContext.class);
		}
		public NumericContext numeric(int i) {
			return getRuleContext(NumericContext.class,i);
		}
		public AssignmentExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).enterAssignmentExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).exitAssignmentExpression(this);
		}
	}

	public final AssignmentExpressionContext assignmentExpression() throws RecognitionException {
		AssignmentExpressionContext _localctx = new AssignmentExpressionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_assignmentExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			match(T__0);
			setState(55);
			match(LPAR);
			setState(56);
			((AssignmentExpressionContext)_localctx).s = match(STRINGCHAR);
			setState(57);
			match(T__1);
			setState(58);
			((AssignmentExpressionContext)_localctx).n1 = numeric((((AssignmentExpressionContext)_localctx).n1!=null?_input.getText(((AssignmentExpressionContext)_localctx).n1.start,((AssignmentExpressionContext)_localctx).n1.stop):null));
			setState(59);
			match(T__1);

					map.put((((AssignmentExpressionContext)_localctx).s!=null?((AssignmentExpressionContext)_localctx).s.getText():null), new Integer(((AssignmentExpressionContext)_localctx).n1.value));
				
			setState(61);
			((AssignmentExpressionContext)_localctx).n2 = numeric((((AssignmentExpressionContext)_localctx).n2!=null?_input.getText(((AssignmentExpressionContext)_localctx).n2.start,((AssignmentExpressionContext)_localctx).n2.stop):null));
			setState(62);
			match(RPAR);
					
					((AssignmentExpressionContext)_localctx).value =  ((AssignmentExpressionContext)_localctx).n2.value;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericContext extends ParserRuleContext {
		public String expr;
		public int value;
		public AssignmentExpressionContext e;
		public Token n;
		public ArithmeticExpressionContext p;
		public Token s;
		public AssignmentExpressionContext assignmentExpression() {
			return getRuleContext(AssignmentExpressionContext.class,0);
		}
		public TerminalNode NUMBER() { return getToken(SimpleCalculatorParser.NUMBER, 0); }
		public ArithmeticExpressionContext arithmeticExpression() {
			return getRuleContext(ArithmeticExpressionContext.class,0);
		}
		public TerminalNode STRINGCHAR() { return getToken(SimpleCalculatorParser.STRINGCHAR, 0); }
		public NumericContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public NumericContext(ParserRuleContext parent, int invokingState, String expr) {
			super(parent, invokingState);
			this.expr = expr;
		}
		@Override public int getRuleIndex() { return RULE_numeric; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).enterNumeric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SimpleCalculatorListener ) ((SimpleCalculatorListener)listener).exitNumeric(this);
		}
	}

	public final NumericContext numeric(String expr) throws RecognitionException {
		NumericContext _localctx = new NumericContext(_ctx, getState(), expr);
		enterRule(_localctx, 4, RULE_numeric);
		try {
			setState(75);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(65);
				((NumericContext)_localctx).e = assignmentExpression();
				((NumericContext)_localctx).value =  ((NumericContext)_localctx).e.value;
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(68);
				((NumericContext)_localctx).n = match(NUMBER);
				((NumericContext)_localctx).value =  Integer.parseInt((((NumericContext)_localctx).n!=null?((NumericContext)_localctx).n.getText():null));
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(70);
				((NumericContext)_localctx).p = arithmeticExpression();
				((NumericContext)_localctx).value =  ((NumericContext)_localctx).p.value;
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(73);
				((NumericContext)_localctx).s = match(STRINGCHAR);

						Integer v = (Integer)map.get((((NumericContext)_localctx).s!=null?((NumericContext)_localctx).s.getText():null));
				        if ( v!=null ) ((NumericContext)_localctx).value =  v.intValue();       
				    
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\17P\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2\64\n\2\f\2\16\2\67\13\2"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\5\4N\n\4\3\4\2\2\5\2\4\6\2\2T\2\65\3\2\2\2\48\3\2\2\2\6"+
		"M\3\2\2\2\b\t\7\3\2\2\t\n\7\t\2\2\n\13\7\13\2\2\13\f\7\4\2\2\f\r\5\6\4"+
		"\2\r\16\7\4\2\2\16\17\b\2\1\2\17\20\5\6\4\2\20\21\7\n\2\2\21\22\b\2\1"+
		"\2\22\64\3\2\2\2\23\24\7\5\2\2\24\25\7\t\2\2\25\26\5\6\4\2\26\27\7\4\2"+
		"\2\27\30\5\6\4\2\30\31\7\n\2\2\31\32\b\2\1\2\32\64\3\2\2\2\33\34\7\6\2"+
		"\2\34\35\7\t\2\2\35\36\5\6\4\2\36\37\7\4\2\2\37 \5\6\4\2 !\7\n\2\2!\""+
		"\b\2\1\2\"\64\3\2\2\2#$\7\7\2\2$%\7\t\2\2%&\5\6\4\2&\'\7\4\2\2\'(\5\6"+
		"\4\2()\7\n\2\2)*\b\2\1\2*\64\3\2\2\2+,\7\b\2\2,-\7\t\2\2-.\5\6\4\2./\7"+
		"\4\2\2/\60\5\6\4\2\60\61\7\n\2\2\61\62\b\2\1\2\62\64\3\2\2\2\63\b\3\2"+
		"\2\2\63\23\3\2\2\2\63\33\3\2\2\2\63#\3\2\2\2\63+\3\2\2\2\64\67\3\2\2\2"+
		"\65\63\3\2\2\2\65\66\3\2\2\2\66\3\3\2\2\2\67\65\3\2\2\289\7\3\2\29:\7"+
		"\t\2\2:;\7\13\2\2;<\7\4\2\2<=\5\6\4\2=>\7\4\2\2>?\b\3\1\2?@\5\6\4\2@A"+
		"\7\n\2\2AB\b\3\1\2B\5\3\2\2\2CD\5\4\3\2DE\b\4\1\2EN\3\2\2\2FG\7\f\2\2"+
		"GN\b\4\1\2HI\5\2\2\2IJ\b\4\1\2JN\3\2\2\2KL\7\13\2\2LN\b\4\1\2MC\3\2\2"+
		"\2MF\3\2\2\2MH\3\2\2\2MK\3\2\2\2N\7\3\2\2\2\5\63\65M";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}