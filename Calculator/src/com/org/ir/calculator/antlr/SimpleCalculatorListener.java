// Generated from etc\SimpleCalculator.g4 by ANTLR 4.5.3

package com.org.ir.calculator.antlr;

import java.util.HashMap;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SimpleCalculatorParser}.
 */
public interface SimpleCalculatorListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SimpleCalculatorParser#arithmeticExpression}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpression(SimpleCalculatorParser.ArithmeticExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SimpleCalculatorParser#arithmeticExpression}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpression(SimpleCalculatorParser.ArithmeticExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SimpleCalculatorParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpression(SimpleCalculatorParser.AssignmentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SimpleCalculatorParser#assignmentExpression}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpression(SimpleCalculatorParser.AssignmentExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SimpleCalculatorParser#numeric}.
	 * @param ctx the parse tree
	 */
	void enterNumeric(SimpleCalculatorParser.NumericContext ctx);
	/**
	 * Exit a parse tree produced by {@link SimpleCalculatorParser#numeric}.
	 * @param ctx the parse tree
	 */
	void exitNumeric(SimpleCalculatorParser.NumericContext ctx);
}