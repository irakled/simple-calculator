package com.org.ir.calculator;

import com.org.ir.calculator.antlr.SimpleCalculatorLexer;
import com.org.ir.calculator.antlr.SimpleCalculatorParser;
import com.org.ir.calculator.antlr.SimpleCalculatorParser.ArithmeticExpressionContext;

import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.commons.logging.Log;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;

public class Calculator 
{
	
	private final static Log log = LogFactory.getLog(Calculator.class);

	public static void main(String[] args) 
	{
		Calculator cal = new Calculator();
		String expression = null;
		String verbose = null;
		
		if((args.length == 0) || (args.length > 2))
		{
			log.error("Invalid number of arguments.");
			System.exit(1);
		}
		else if(args.length == 2)
		{
			verbose = args[1];
		}
		
		expression = args[0];
		
		if(verbose != null)
		{
			switch(verbose)
			{
				case "-info":
					LogManager.getRootLogger().setLevel(Level.INFO);
					break;
				case "-debug":
					LogManager.getRootLogger().setLevel(Level.DEBUG);
					break;
				case "-error":
					LogManager.getRootLogger().setLevel(Level.ERROR);
					break;
				case "-all":
					LogManager.getRootLogger().setLevel(Level.ALL);
					break;
				default:
					log.error("Invalid verbose level argument. Valid options are -info, -debug -error or -all.");
					System.exit(1);
			}
		}
		
		try
		{
			cal.printArithmeticExpression(expression);
		}
		catch (RecognitionException re)
		{
			log.error(re.fillInStackTrace());
		}
		catch(Exception e)
		{
			log.error(e.getMessage());
		}
	}
	
	private void printArithmeticExpression(String expression) throws Exception
	{
		log.debug("Starting calculation.");
		SimpleCalculatorLexer lexer = new SimpleCalculatorLexer(new ANTLRInputStream(expression));		 
	    CommonTokenStream tokens = new CommonTokenStream(lexer);	 
	    SimpleCalculatorParser parser = new SimpleCalculatorParser(tokens);    
	    ArithmeticExpressionContext mathSentenceContext = parser.arithmeticExpression();	    
	    
	    log.info("Value for expression " + expression + " is " + mathSentenceContext.value);
	    System.out.println(mathSentenceContext.value);
	}
}
